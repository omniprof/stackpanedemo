package com.kenfogel.stackpane_demo;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A demo of how to stack panes in a StackPane
 *
 * @author kfogel
 */
public class StackPaneDemo extends Application {

    // Real programmers use logging, not System.out.println
    private final static Logger LOG = LoggerFactory.getLogger(StackPaneDemo.class);

    // This is the StackPane that will hold all the following BorderPanes
    private StackPane stacker;
    private BorderPane pane1;
    private BorderPane pane2;
    private BorderPane pane3;
    private BorderPane pane4;

    /**
     * Where it all begins
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    /**
     * Build the basic layout
     *
     * @param stage
     * @throws Exception
     */
    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("JavaFX Stackpane Demo");
        Scene scene = new Scene(createGUI(), 300, 275);
        stage.setScene(scene);
        stage.show();
    }

    /**
     * Here is the specific GUI design
     *
     * @return BorderPane with all the components
     */
    private BorderPane createGUI() {
        // This is the pane that will go into the Scene
        BorderPane root = new BorderPane();

        // Create the StackPane and place it in the ecenter of the BorderPane
        stacker = new StackPane();
        root.setCenter(stacker);

        // make the four panes we wish to stack
        makePane1();
        makePane2();
        makePane3();
        makePane4();
        // Add all the panes to the stackpane
        stacker.getChildren().addAll(pane1, pane2, pane3, pane4);

        // Create an HBox with four buttons to switch beteen panes
        Button button1 = new Button("1");
        Button button2 = new Button("2");
        Button button3 = new Button("3");
        Button button4 = new Button("4");

        // Set the event handlers for the buttons
        button1.setOnAction(this::make1Top);
        button2.setOnAction(this::make2Top);
        button3.setOnAction(this::make3Top);
        button4.setOnAction(this::make4Top);

        // Create the HBox to hold the pane selelction buttons
        HBox hbox = new HBox();
        hbox.setStyle("-fx-background-color: fuchsia;");
        hbox.setPadding(new Insets(25, 25, 25, 25));
        hbox.setSpacing(10.0);
        hbox.setAlignment(Pos.CENTER);
        hbox.getChildren().addAll(button1, button2, button3, button4);
        root.setTop(hbox);

        return root;
    }

    /**
     * Create a pane with a label in the middle and an HBox with a button in the
     * bottom. All four panes are identical except for the label and button
     * label. This is unusual as the different panes are normally quite
     * different from each other.
     */
    private void makePane1() {
        pane1 = new BorderPane();
        pane1.setCenter(new Label("Pane 1"));
        pane1.setVisible(false);
        Button pbutton1 = new Button("Pane 1");
        pbutton1.setOnAction(this::makep1);

        HBox hbox = new HBox();
        hbox.setStyle("-fx-background-color: yellow;");
        hbox.setPadding(new Insets(25, 25, 25, 25));
        hbox.setSpacing(10.0);
        hbox.setAlignment(Pos.CENTER);
        hbox.getChildren().add(pbutton1);
        pane1.setBottom(hbox);
    }

    /**
     * Create a pane with a label in the middle and an HBox with a button in the
     * bottom
     */
    private void makePane2() {
        pane2 = new BorderPane();
        pane2.setCenter(new Label("Pane 2"));
        pane2.setVisible(false);
        Button pbutton2 = new Button("Pane 2");
        pbutton2.setOnAction(this::makep2);

        HBox hbox = new HBox();
        hbox.setStyle("-fx-background-color: aqua;");
        hbox.setPadding(new Insets(25, 25, 25, 25));
        hbox.setSpacing(10.0);
        hbox.setAlignment(Pos.CENTER);
        hbox.getChildren().add(pbutton2);
        pane2.setBottom(hbox);
    }

    /**
     * Create a pane with a label in the middle and an HBox with a button in the
     * bottom
     */
    private void makePane3() {
        pane3 = new BorderPane();
        pane3.setCenter(new Label("Pane 3"));
        pane3.setVisible(false);
        Button pbutton3 = new Button("Pane 3");
        pbutton3.setOnAction(this::makep3);

        HBox hbox = new HBox();
        hbox.setStyle("-fx-background-color: lime;");
        hbox.setSpacing(10.0);
        hbox.setPadding(new Insets(25, 25, 25, 25));
        hbox.setAlignment(Pos.CENTER);
        hbox.getChildren().add(pbutton3);
        pane3.setBottom(hbox);
    }

    /**
     * Create a pane with a label in the middle and an HBox with a button in the
     * bottom
     */
    private void makePane4() {
        pane4 = new BorderPane();
        pane4.setCenter(new Label("Pane 4"));
        pane4.setVisible(true);
        Button pbutton4 = new Button("Pane 4");
        pbutton4.setOnAction(this::makep4);

        HBox hbox = new HBox();
        hbox.setStyle("-fx-background-color: silver;");
        hbox.setPadding(new Insets(25, 25, 25, 25));
        hbox.setSpacing(10.0);
        hbox.setAlignment(Pos.CENTER);
        hbox.getChildren().add(pbutton4);
        pane4.setBottom(hbox);
    }

    /**
     * The event handler for when a pane selection button is pressed
     */
    private void make1Top(ActionEvent e) {
        pane1.toFront();
        pane1.setVisible(true);
        closeOtherPanes(pane1);
    }

    /**
     * The event handler for when a pane selection button is pressed
     */
    private void make2Top(ActionEvent e) {
        pane2.toFront();
        pane2.setVisible(true);
        closeOtherPanes(pane2);
    }

    /**
     * The event handler for when a pane selection button is pressed
     */
    private void make3Top(ActionEvent e) {
        pane3.toFront();
        pane3.setVisible(true);
        closeOtherPanes(pane3);
    }

    /**
     * The event handler for when a pane selection button is pressed
     */
    private void make4Top(ActionEvent e) {
        pane4.toFront();
        pane4.setVisible(true);
        closeOtherPanes(pane4);
    }

    /**
     * setVisible to false for all but the pane parameter
     *
     * @param paneTokeep The pane to preserve
     */
    private void closeOtherPanes(Node paneTokeep) {
        stacker.getChildren().stream().filter((pane) -> (pane != paneTokeep)).forEachOrdered((pane) -> {
            pane.setVisible(false);
        });
    }

    /**
     * Display a log message when a button belonging to a specific pane is
     * pressed
     *
     * @param e
     */
    private void makep1(ActionEvent e) {
        LOG.info("pane 1 button");
    }

    /**
     * Display a log message when a button belonging to a specific pane is
     * pressed
     *
     * @param e
     */
    private void makep2(ActionEvent e) {
        LOG.info("pane 2 button");
    }

    /**
     * Display a log message when a button belonging to a specific pane is
     * pressed
     *
     * @param e
     */
    private void makep3(ActionEvent e) {
        LOG.info("pane 3 button");
    }

    /**
     * Display a log message when a button belonging to a specific pane is
     * pressed
     *
     * @param e
     */
    private void makep4(ActionEvent e) {
        LOG.info("pane 4 button");
    }
}
